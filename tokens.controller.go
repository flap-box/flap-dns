package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

// Tokens - Controller for /tokens
type Tokens struct {
}

// GetTokensURI - URI params for GET /tokens/:email
type GetTokensURI struct {
	Email string `uri:"email" binding:"required,email,max=255,min=6"`
}

// GetTokensParams - Query params for GET /tokens/:email
type GetTokensParams struct {
	Format string `form:"format"`
}

// GetTokensBody - JSON body for GET /tokens/:email
type GetTokensBody struct {
	Token string `json:"token" binding:"required,len=30"`
}

// Get - GET /tokens/:email
func (t Tokens) Get(c *gin.Context) {
	// Allow request from any site.
	c.Header("Access-Control-Allow-Origin", "*")

	var uri GetTokensURI
	err := c.BindUri(&uri)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	var body GetTokensBody
	if c.Request.ContentLength > 0 {
		err = c.BindJSON(&body)

		if err != nil {
			c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
			return
		}
	}

	var params GetTokensParams
	err = c.BindQuery(&params)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	db := GetDBConnection()
	defer db.Close()

	// Get domains where email = email and delegate = "" or delegate = email
	var domains []Domain
	db.Where("email = ? AND delegate = ?", uri.Email, "").Or("delegate = ?", uri.Email).Find(&domains)

	if db.Error != nil {
		c.Error(db.Error)
		c.String(http.StatusInternalServerError, "failed to get your email info")
		return
	}

	count, err := GetAllowedDomainsForStripeEmail(uri.Email)

	if err != nil {
		c.Error(err)
		c.String(http.StatusInternalServerError, "failed to get your domains info")
		return
	}

	tokens := make(map[string]string)

	// Generate a tmp_token for each domains
	for _, domain := range domains {
		token, hash, err := GenerateToken()

		if err != nil {
			c.Error(err)
			c.String(http.StatusInternalServerError, "failed to generate new tokens")
			return
		}

		domain.TmpToken = hash
		domain.CreatedDate = time.Now()
		db.Save(&domain)

		if db.Error != nil {
			c.Error(db.Error)
			c.String(http.StatusInternalServerError, "failed to generate new tokens")
			return
		}

		count--
		tokens[domain.Name] = token
	}

	// Delete tokens in tokens base for the email
	db.Where("email = ?", uri.Email).Delete(Token{})

	if db.Error != nil {
		c.Error(db.Error)
		c.String(http.StatusInternalServerError, "failed to get your token info")
		return
	}

	// Create token entry in token bases for each remaining domains
	for count > 0 {
		token, hash, err := GenerateToken()

		if err != nil {
			c.Error(err)
			c.String(http.StatusInternalServerError, "failed to generate new tokens")
			return
		}

		count--
		tokens[fmt.Sprintf("Empty %d", count)] = token

		db.Create(&Token{
			Token:       hash,
			CreatedDate: time.Now(),
			Email:       uri.Email,
		})

		if db.Error != nil {
			c.Error(db.Error)
			c.String(http.StatusInternalServerError, "failed to save your new tokens")
			return
		}
	}

	if len(tokens) == 0 {
		c.String(http.StatusNotFound, "Votre mail n'est pas associé à des tokens.")
		return
	}

	// Don't send mail during tests
	if flag.Lookup("test.v") != nil || gin.Mode() == gin.DebugMode {
		c.JSON(http.StatusOK, tokens)
		return
	}

	// Send the tokens or render the key if `json` is set.
	fmt.Println(uri)
	if params.Format == "json" && body.Token == os.Getenv("MASTER_TOKEN") {
		c.JSON(http.StatusOK, tokens)
	} else {
		SendTokens(uri.Email, tokens)
		c.String(http.StatusOK, "Vos tokens ont été envoyés.")
	}

	if count < 0 {
		panic("you have to mutch domains")
	}
}
