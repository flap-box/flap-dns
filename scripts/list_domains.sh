#!/bin/bash

set -eu

mapfile -t domains_from_sqlite < <(sqlite3 /opt/flap-dns.db "select name from domains where deleted_at is null;")
mapfile -t domains_from_bind < <(cat /var/cache/bind/flap.zones.d/db.catalog.flap.id | grep PTR | cut -d ' ' -f3 | cut -d '.' -f-3)


domains=("${domains_from_sqlite[@]}" "${domains_from_bind[@]}")

IFS=$'\n' sorted=($(sort -u <<<"${domains[*]}"))
unset IFS

for domain in ${sorted[*]}
do
        if [ "$domain" == "flap.id." ]
        then
                continue
        fi

        status="OK"

        if [ ! -f "/var/cache/bind/flap.zones.d/db.$domain" ]
        then
                status="Missing zone file"
        elif [ "$(cat "/var/cache/bind/flap.zones.d/db.$domain")" == "" ]
        then
                status="Zone file is empty"
        fi

        exist=$(sqlite3 /opt/flap-dns.db "select name from domains where name = '$domain';")
        if [ "$exist" == "" ]
        then
                status="Missing from sqlite"
        fi

        echo "$domain - $status"
done
