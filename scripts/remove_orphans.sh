#!/bin/bash

set -eu

for domain in $(/opt/scripts/list_orphans.sh | grep "Missing zone file" | cut -d '-' -f1)
do
	sqlite3 /opt/flap-dns.db "delete from domains where name = '$domain';"
done
