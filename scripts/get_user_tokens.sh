#!/bin/bash

set -eu

email=$1

source /opt/.env

wget --method GET \
	--header "Content-Type: application/json" \
	--body-data "{
			\"token\": \"$MASTER_TOKEN\"
		}" \
	--quiet \
	--output-document=- \
	--content-on-error \
	"https://flap.id/tokens/$email?format=json"
