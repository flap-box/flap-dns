#!/bin/bash

set -eu

domain=$1

echo Domain: "$(sqlite3 /opt/flap-dns.db "select name from domains where name = '$domain';")"
echo Creation date: "$(sqlite3 /opt/flap-dns.db "select created_date from domains where name = '$domain';")"
echo Deletion date: "$(sqlite3 /opt/flap-dns.db "select deleted_at from domains where name = '$domain';")"
echo Owner: "$(sqlite3 /opt/flap-dns.db "select email from domains where name = '$domain';")"
echo Delegate: "$(sqlite3 /opt/flap-dns.db "select delegate from domains where name = '$domain';")"
echo Token: "$(sqlite3 /opt/flap-dns.db "select token from domains where name = '$domain';")"
echo ---
echo IP4: "$(sqlite3 /opt/flap-dns.db "select ip4 from domains where name = '$domain';")"
echo IP6: "$(sqlite3 /opt/flap-dns.db "select ip6 from domains where name = '$domain';")"
echo DKIM: "$(sqlite3 /opt/flap-dns.db "select dkim from domains where name = '$domain';")"
echo ---
echo Serial: "$(sqlite3 /opt/flap-dns.db "select serial from domains where name = '$domain';")"
echo SerialDate: "$(sqlite3 /opt/flap-dns.db "select serial_date from domains where name = '$domain';")"
echo ---
rndc zonestatus "$domain" || true
rndc showzone "$domain" || true
