#!/bin/bash

set -eu

domain=$1

source /opt/.env

wget --method DELETE \
	--header "Content-Type: application/json" \
	--body-data "{
			\"token\": \"$MASTER_TOKEN\"
		}" \
	--quiet \
	--output-document=- \
	--content-on-error \
	"https://flap.id/domains/$domain"
