package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
)

// Domain - Structure of a domain in the database
type Domain struct {
	gorm.Model
	Name        string `gorm:"size:71;not null;unique"`
	Email       string `gorm:"not null"`
	Delegate    string
	Token       string `gorm:"size:60"`
	TmpToken    string `gorm:"size:60"`
	CreatedDate time.Time
	IP4         string `gorm:"size:15"`
	IP6         string `gorm:"size:39"`
	DKIM        string `gorm:"size:500"`
	Serial      int
	SerialDate  time.Time
}

// DomainPatch -
type DomainPatch struct {
	Name     string
	Delegate string
	IP4      string
	IP6      string
	DKIM     string
}

// IsFlapID Check that the domain name ends with .flap.id
var IsFlapID validator.Func = func(fl validator.FieldLevel) bool {
	domain, ok := fl.Field().Interface().(string)

	if !ok {
		return false
	}

	return strings.HasSuffix(domain, ".flap.id")
}

// IsNotInFlapIDBlacklist Check that the domain is not on the blacklist
var IsNotInFlapIDBlacklist validator.Func = func(fl validator.FieldLevel) bool {
	domain, ok := fl.Field().Interface().(string)

	if !ok {
		return false
	}

	blacklist := map[string]bool{
		"ns1.flap.id":     false,
		"ns2.flap.id":     false,
		"ns3.flap.id":     false,
		"ns4.flap.id":     false,
		"ns5.flap.id":     false,
		"catalog.flap.id": false,
		"dev.flap.id":     false,
		"test.flap.id":    false,
		"demo.flap.id":    false,
		"mail.flap.id":    false,
	}

	_, isInBlacklist := blacklist[domain]

	return !isInBlacklist
}

func getDomainFromDB(name string) (Domain, error) {
	var domain Domain

	db := GetDBConnection()
	defer db.Close()

	db.First(&domain, "name = ?", name)

	if db.Error != nil {
		return domain, db.Error
	}

	return domain, nil
}

// CheckDomainTokenCoupleValidity - Check the validity of a domain/token couple and return the domain
func CheckDomainTokenCoupleValidity(name string, token string) (Domain, error) {
	log.Println("Checking domain/token couple validity for: " + name)

	var emptyDomain Domain
	domain, err := getDomainFromDB(name)

	if err != nil {
		return emptyDomain, err
	}

	if TokenEqualHash(token, domain.Token) {
		return domain, nil
	}

	if TokenEqualHash(token, domain.TmpToken) && domain.CreatedDate.Unix() > time.Now().AddDate(0, 0, -1).Unix() {
		// 	Set token to tmp_token
		domain.Token = domain.TmpToken
		// 	Empty tmp_token
		domain.TmpToken = ""

		return domain, nil
	}

	// Allow a master token to administrate domains
	if token == os.Getenv("MASTER_TOKEN") {
		return domain, nil
	}

	return emptyDomain, fmt.Errorf("The token is invalid")
}

// RegisterDomain - Save a new domain to the database
func RegisterDomain(name string, token string) FlapStatus {
	log.Println("Start registering " + name)

	// Connect to db
	db := GetDBConnection()
	defer db.Close()

	// Check availability of the domain name
	var domain Domain
	db.First(&domain, "name = ?", name)

	if db.Error != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   db.Error,
			Message: "failed to get the domain info",
		}
	}

	if domain.ID != 0 {
		if TokenEqualHash(token, domain.Token) || TokenEqualHash(token, domain.TmpToken) {
			log.Println(name + " is already owned by the token")

			return FlapStatus{
				Code:    http.StatusOK,
				Message: "you already own the domain",
			}
		}

		log.Println(name + " is owned by someone else")
		return FlapStatus{
			Code:    http.StatusForbidden,
			Message: "the domain name is already registered",
		}
	}

	log.Println(name + " is available")

	// Check token validity
	tokenInfo, hash, err := GetPendingTokenInfo(token)

	if err != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   err,
			Message: "failed to get your token info",
		}
	}

	if tokenInfo.ID == 0 {
		log.Println("The token is invalid for " + name)
		return FlapStatus{
			Code:    http.StatusUnauthorized,
			Message: "your token is invalid",
		}
	}

	log.Println("The token is valid for " + name)

	// 	Register the domain and spend the token
	log.Println("Saving the new domain '" + name + "' in database with the token")
	db.Create(&Domain{
		Name:  name,
		Email: tokenInfo.Email,
		Token: hash,
	})

	if db.Error != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   db.Error,
			Message: "failed to register the domain",
		}
	}

	log.Println(name + " is saved in database")

	// Spend the token
	log.Println("Deleting the token from the 'tokens' db")
	db.Delete(&tokenInfo)

	if db.Error != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   db.Error,
			Message: "failed to get your token info",
		}
	}

	log.Println("Token is deleted")

	log.Println("Done registering " + name)

	return FlapStatus{
		Code:    http.StatusCreated,
		Message: "the domain name was correctly registered",
	}
}

// UpdateDomain - Update a domain in the database
func UpdateDomain(name string, token string, update DomainPatch) FlapStatus {
	// Check token validity
	domain, err := CheckDomainTokenCoupleValidity(name, token)

	if err != nil {
		code := http.StatusInternalServerError
		if err.Error() == "The token is invalid" {
			code = http.StatusUnauthorized
		}

		return FlapStatus{
			Code:    code,
			Error:   err,
			Message: "failed to get the domain info",
		}
	}

	if domain.ID == 0 {
		return FlapStatus{
			Code:    http.StatusUnauthorized,
			Error:   err,
			Message: "your token is invalid",
		}
	}

	// Update domain
	if update.Name != "" {
		log.Println("Migrating " + domain.Name + " to " + update.Name)
		// Delete old zone file
		err = DeleteZone(domain.Name)

		if err != nil {
			return FlapStatus{
				Code:    http.StatusInternalServerError,
				Error:   err,
				Message: "failed to remove you domain",
			}
		}

		// 	Rename domain to new domain
		domain.Name = update.Name
	}

	if update.IP4 != "" || update.IP6 != "" || update.DKIM != "" {
		if update.IP4 != "" {
			log.Println("Updating IPv4 for the domain: " + domain.Name)
			domain.IP4 = update.IP4
		}

		if update.IP6 != "" {
			log.Println("Updating IPv6 for the domain: " + domain.Name)
			domain.IP6 = update.IP6
		}

		if update.DKIM != "" {
			log.Println("Updating DKIM for the domain: " + domain.Name)
			domain.DKIM = update.DKIM
		}

		// 	Update zone file
		err = GenerateZone(&domain)

		if err != nil {
			return FlapStatus{
				Code:    http.StatusInternalServerError,
				Error:   err,
				Message: "failed to apply your changes",
			}
		}
	}

	if update.Delegate != "" {
		log.Println("Updating delegate for the domain: " + domain.Name)
		// 	Update delegate
		domain.Delegate = update.Delegate
		// 	Generate new token
		token, hash, err := GenerateToken()
		if err != nil {
			return FlapStatus{
				Code:    http.StatusInternalServerError,
				Error:   err,
				Message: "failed create a new token for the delegate",
			}
		}
		// 	Set token to the new token
		domain.Token = hash
		// 	Empty tmp_token
		domain.TmpToken = ""
		// 	Send token to delegate
		SendTokens(domain.Delegate, map[string]string{domain.Name: token})
	}

	db := GetDBConnection()
	defer db.Close()

	// Save domain in db
	db.Save(&domain)

	if db.Error != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   db.Error,
			Message: "failed to save your changes",
		}
	}

	return FlapStatus{
		Code:    http.StatusOK,
		Message: "the domain name was correctly updated",
	}
}

// DeleteDomain - Update a domain in the database
func DeleteDomain(name string, token string) FlapStatus {
	log.Println("Start deleting " + name)

	// Check token validity
	domain, err := CheckDomainTokenCoupleValidity(name, token)

	if err != nil {
		code := http.StatusInternalServerError
		if err.Error() == "The token is invalid" {
			code = http.StatusUnauthorized
		}

		return FlapStatus{
			Code:    code,
			Error:   err,
			Message: "failed to get the domain info",
		}
	}

	if domain.ID == 0 {
		return FlapStatus{
			Code:    http.StatusUnauthorized,
			Error:   err,
			Message: "your token is invalid",
		}
	}

	log.Println("The token is valid for " + name)

	log.Println("Deleting zone file for " + domain.Name)
	err = DeleteZone(domain.Name)

	if err != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   err,
			Message: "failed to remove you domain",
		}
	}

	db := GetDBConnection()
	defer db.Close()

	// Save domain in db
	db.Delete(&domain)

	if db.Error != nil {
		return FlapStatus{
			Code:    http.StatusInternalServerError,
			Error:   db.Error,
			Message: "failed to save domain deletion request",
		}
	}

	return FlapStatus{
		Code:    http.StatusOK,
		Message: "the domain name was correctly deleted",
	}
}
