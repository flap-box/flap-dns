package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Domains - Controller for /domains
type Domains struct {
}

// PostDomainsBody - JSON body params for POST /domains
type PostDomainsBody struct {
	// Max size of a flap.id domain name is 71: 63 for the label + ".flap.id"
	// See: https://stackoverflow.com/questions/32290167/what-is-the-maximum-length-of-a-dns-name
	Domain string `json:"name" binding:"required,fqdn,isflapid,isnotinflapidblacklist,max=71,min=11"`
	Token  string `json:"token" binding:"required,len=30"`
}

// PatchDomainsBody - JSON body params for PATCH /domains/:domainname
type PatchDomainsBody struct {
	Name     string `json:"name" binding:"omitempty,fqdn,isflapid,isnotinflapidblacklist,max=71,min=11"`
	IP4      string `json:"ip4" binding:"omitempty,ipv4"`
	IP6      string `json:"ip6" binding:"omitempty,ipv6"`
	DKIM     string `json:"dkim" binding:"omitempty,max=500"`
	Delegate string `json:"delegate" binding:"omitempty,max=255,min=6"`
	Token    string `json:"token" binding:"required,len=30"`
}

// PatchDomainsURI - URI params for PATCH /domains/:domainname
type PatchDomainsURI struct {
	Domain string `uri:"name" binding:"required,fqdn,max=71,min=11,isflapid"`
}

// DeleteDomainsBody - JSON body params for DELETE /domains/:domainname
type DeleteDomainsBody struct {
	Token string `json:"token" binding:"required,len=30"`
}

// DeleteDomainsURI - URI params for DELETE /domains/:domainname
type DeleteDomainsURI struct {
	Domain string `uri:"name" binding:"required,fqdn,max=71,min=11,isflapid"`
}

// Post - POST /domains
func (d Domains) Post(c *gin.Context) {
	// Get params
	var body PostDomainsBody
	err := c.BindJSON(&body)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	flapStatus := RegisterDomain(body.Domain, body.Token)

	if flapStatus.Error != nil {
		c.Error(flapStatus.Error)
	}

	c.String(flapStatus.Code, flapStatus.Message)
}

// Patch - PATH /domains/:domainname
func (d Domains) Patch(c *gin.Context) {
	// Get params
	var uri PatchDomainsURI
	err := c.BindUri(&uri)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	var body PatchDomainsBody
	err = c.BindJSON(&body)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	flapStatus := UpdateDomain(uri.Domain, body.Token, DomainPatch{
		Name:     body.Name,
		Delegate: body.Delegate,
		IP4:      body.IP4,
		IP6:      body.IP6,
		DKIM:     body.DKIM,
	})

	if flapStatus.Error != nil {
		c.Error(flapStatus.Error)
	}

	c.String(flapStatus.Code, flapStatus.Message)
}

// Delete - DELETE /domains/:domainname
func (d Domains) Delete(c *gin.Context) {
	// Get params
	var uri DeleteDomainsURI
	err := c.BindUri(&uri)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	var body DeleteDomainsBody
	err = c.BindJSON(&body)

	if err != nil {
		c.String(http.StatusBadRequest, "failed to understand your request: %v", err)
		return
	}

	flapStatus := DeleteDomain(uri.Domain, body.Token)

	if flapStatus.Error != nil {
		c.Error(flapStatus.Error)
	}

	c.String(flapStatus.Code, flapStatus.Message)
}
