package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

type GetTokenBody struct {
	Domain string `json:"domain"`
	Token  string `json:"token"`
}

var domain1 = "mydomain.flap.id"
var domain2 = "mydomain2.flap.id"

var router = gin.Default()

func TestMain(m *testing.M) {
	InitDB()
	setupValidators()
	setupRoutes(router)

	// Load .env
	err := godotenv.Load(".env", "release.env")
	log.Println(err)

	println("Setting email count")
	MockEmailCount = map[string]int{
		"one@flap.id": 1,
		"two@flap.id": 2,
	}

	os.Exit(m.Run())
}

func Before(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	println("Cleaning DB")
	os.Remove("flap-dns.db")
	InitDB()

	println("Deleting zone file")
	os.RemoveAll("./flap.zones.d")
}

func After() {
	println("Tear down")
}

func TestThatGettingTokensWorks(t *testing.T) {
	Before(t)
	_, w := getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
	After()
}

func TestThatGettingTokensIsNotAllowedForUnknownEmail(t *testing.T) {
	Before(t)
	w := request("GET", "/tokens/zero@flap.id", nil)
	assert.Equal(t, http.StatusNotFound, w.Code)
	zoneFileIsNotCreated(t, domain1)
	After()
}

func TestThatRegisteringDomainWorks(t *testing.T) {
	Before(t)
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})
	assert.Equal(t, http.StatusCreated, w.Code)
	_, w = getToken(t, "one@flap.id", domain1)
	containsOnly(t, w, domain1)
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegistering2DomainWorks(t *testing.T) {
	Before(t)
	token, _ := getToken(t, "two@flap.id", "Empty 1")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})
	assert.Equal(t, http.StatusCreated, w.Code)
	token, _ = getToken(t, "two@flap.id", "Empty 0")
	w = registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain2,
	})
	assert.Equal(t, http.StatusCreated, w.Code)
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringDomainWithWrongTokenDoesNotWorks(t *testing.T) {
	Before(t)
	wrongToken, _ := getToken(t, "one@flap.id", "Empty 0")
	getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  wrongToken,
		Domain: domain1,
	})
	assert.Equal(t, http.StatusUnauthorized, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringDomainNotFlapIdDoesNotWorks(t *testing.T) {
	Before(t)
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: "my.test.com",
	})
	assert.Equal(t, http.StatusBadRequest, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringBlacklistedDomainDoesNotWorks(t *testing.T) {
	Before(t)
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: "test.flap.id",
	})
	assert.Equal(t, http.StatusBadRequest, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringDomainWithTwoOrLessCharDoesNotWorks(t *testing.T) {
	Before(t)
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: "12.flap.id",
	})
	assert.Equal(t, http.StatusBadRequest, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringEmptyDomainDoesNotWorks(t *testing.T) {
	Before(t)
	w := registerDomain(t, PostDomainsBody{
		Token:  "",
		Domain: "",
	})
	assert.Equal(t, http.StatusBadRequest, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringSecondDomainDosNotWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Register the domain test2.flap.id
	token, _ = getToken(t, "one@flap.id", domain1)
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain2,
	})
	assert.Equal(t, http.StatusUnauthorized, w.Code)
	_, w = getToken(t, "one@flap.id", domain1)
	containsOnly(t, w, domain1)
	zoneFileIsNotCreated(t, domain1)
}

func TestThatRegisteringAnOwnedDomainDoNotError(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Re-register the domain test.flap.id
	token, _ = getToken(t, "one@flap.id", domain1)
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})
	assert.Equal(t, http.StatusOK, w.Code)
	_, w = getToken(t, "one@flap.id", domain1)
	containsOnly(t, w, domain1)
	zoneFileIsNotCreated(t, domain1)
}

func TestThatMigratingDomainWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Migrate the domain test.flap.id to test2.flap.id
	token, _ = getToken(t, "one@flap.id", domain1)
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: token,
		Name:  domain2,
	})
	assert.Equalf(t, http.StatusOK, w.Code, w.Body.String())
	_, w = getToken(t, "one@flap.id", domain2)
	containsOnly(t, w, domain2)
	zoneFileIsNotCreated(t, domain1)
	zoneFileIsNotCreated(t, domain2)
}

func TestThatUpdatingDomainDNSInfoWorksWhenUsingTheSameToken(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update IP and DKIM for the domain test.flap.id
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: token,
		IP4:   "159.159.159.159",
		DKIM:  "DKIM...",
	})
	assert.Equal(t, http.StatusOK, w.Code)
	_, err := os.Open("./flap.zones.d/db." + domain1)
	assert.NoErrorf(t, err, "The zone file should be created")
}

func TestThatUpdatingDomainDNSInfoWorksWhenUsingTheMasterToken(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update IP and DKIM for the domain test.flap.id
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: os.Getenv("MASTER_TOKEN"),
		IP4:   "159.159.159.159",
		DKIM:  "DKIM...",
	})
	assert.Equal(t, http.StatusOK, w.Code)
	_, err := os.Open("./flap.zones.d/db." + domain1)
	assert.NoErrorf(t, err, "The zone file should be created")
}

func TestThatUpdatingDomainDNSInfoWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update IP and DKIM for the domain test.flap.id
	token, _ = getToken(t, "one@flap.id", domain1)
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: token,
		IP4:   "159.159.159.159",
		DKIM:  "DKIM...",
	})
	assert.Equal(t, http.StatusOK, w.Code)
	_, err := os.Open("./flap.zones.d/db." + domain1)
	assert.NoErrorf(t, err, "The zone file should be created")
}

func TestThatUpdatingDomainDelegateWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update delegate for the domain test.flap.id
	token, _ = getToken(t, "one@flap.id", domain1)
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token:    token,
		Delegate: "two@flap.id",
	})
	assert.Equal(t, http.StatusOK, w.Code)

	// Try to update back with wrong token
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token:    token,
		Delegate: "one@flap.id",
	})
	fmt.Println(w.Body)
	assert.Equal(t, http.StatusUnauthorized, w.Code)

	// Update back
	token, _ = getToken(t, "two@flap.id", domain1)
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token:    token,
		Delegate: "one@flap.id",
	})
	assert.Equal(t, http.StatusOK, w.Code)
	zoneFileIsNotCreated(t, domain1)
}

func TestThatUpdatingWithWrongTokenDoesNotWorks(t *testing.T) {
	Before(t)

	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update test.flap.id with the wrong token
	wrongToken, _ := getToken(t, "one@flap.id", domain1)
	_, w = getToken(t, "one@flap.id", domain1)
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: wrongToken,
	})
	zoneFileIsNotCreated(t, domain1)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestThatDeletingDomainWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Delete the domain
	w = deleteDomain(t, domain1, DeleteDomainsBody{
		Token: token,
	})

	assert.Equal(t, http.StatusOK, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatDeletingDomainAfterUpdateWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update IP and DKIM for the domain test.flap.id
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: token,
		IP4:   "159.159.159.159",
		DKIM:  "DKIM...",
	})

	// Delete test.flap.id
	w = deleteDomain(t, domain1, DeleteDomainsBody{
		Token: token,
	})

	assert.Equal(t, http.StatusOK, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatDeletingDomainWithTheMasterTokenWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Delete the domain
	w = deleteDomain(t, domain1, DeleteDomainsBody{
		Token: os.Getenv("MASTER_TOKEN"),
	})

	assert.Equal(t, http.StatusOK, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, "Empty 0")
	zoneFileIsNotCreated(t, domain1)
}

func TestThatDeletingDomainWithWrongTokenDoesNotWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Delete test.flap.id with the wrong token
	wrongToken, _ := getToken(t, "one@flap.id", domain1)
	_, w = getToken(t, "one@flap.id", domain1)
	w = deleteDomain(t, domain1, DeleteDomainsBody{
		Token: wrongToken,
	})

	assert.Equal(t, http.StatusUnauthorized, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, domain1)
	zoneFileIsNotCreated(t, domain1)
}

func TestThatDeletingDomainAfterUpdateWithWrongTokenDoesNotWorks(t *testing.T) {
	Before(t)
	// Register the domain test.flap.id
	token, _ := getToken(t, "one@flap.id", "Empty 0")
	w := registerDomain(t, PostDomainsBody{
		Token:  token,
		Domain: domain1,
	})

	// Update IP and DKIM for the domain test.flap.id
	w = updateDomain(t, domain1, PatchDomainsBody{
		Token: token,
		IP4:   "159.159.159.159",
		DKIM:  "DKIM...",
	})

	// Delete test.flap.id with the wrong token
	wrongToken, _ := getToken(t, "one@flap.id", domain1)
	_, w = getToken(t, "one@flap.id", domain1)
	w = deleteDomain(t, domain1, DeleteDomainsBody{
		Token: wrongToken,
	})

	assert.Equal(t, http.StatusUnauthorized, w.Code)
	_, w = getToken(t, "one@flap.id", "Empty 0")
	containsOnly(t, w, domain1)
	_, err := os.Open("./flap.zones.d/db." + domain1)
	assert.NoErrorf(t, err, "The zone file should be created")
}

func request(method string, uri string, body io.Reader) *httptest.ResponseRecorder {
	w := httptest.NewRecorder()

	req, _ := http.NewRequest(method, uri, body)
	router.ServeHTTP(w, req)

	return w
}

func getToken(t *testing.T, email string, key string) (string, *httptest.ResponseRecorder) {
	w := request("GET", "/tokens/"+email, nil)

	assert.Equal(t, http.StatusOK, w.Code)

	var body map[string]string
	err := json.Unmarshal(w.Body.Bytes(), &body)

	assert.NoErrorf(t, err, "Error during unmarshalling: '%s'", w.Body)

	return body[key], w
}

func registerDomain(t *testing.T, domainsPost PostDomainsBody) *httptest.ResponseRecorder {
	body, err := json.Marshal(domainsPost)

	assert.NoErrorf(t, err, "Error during marshalling")

	w := request("POST", "/domains", bytes.NewBuffer(body))

	fmt.Println(w.Body)

	return w
}

func updateDomain(t *testing.T, domain string, domainPatches PatchDomainsBody) *httptest.ResponseRecorder {
	body, err := json.Marshal(domainPatches)

	assert.NoErrorf(t, err, "Error during marshalling")

	w := request("PATCH", "/domains/"+domain, bytes.NewBuffer(body))

	fmt.Println(w.Body)

	return w
}

func deleteDomain(t *testing.T, domain string, deleteBody DeleteDomainsBody) *httptest.ResponseRecorder {
	body, err := json.Marshal(deleteBody)

	assert.NoErrorf(t, err, "Error during marshalling")

	w := request("DELETE", "/domains/"+domain, bytes.NewBuffer(body))

	fmt.Println(w.Body)

	return w
}

func containsOnly(t *testing.T, w *httptest.ResponseRecorder, key string) {
	var body map[string]string
	err := json.Unmarshal(w.Body.Bytes(), &body)

	assert.NoErrorf(t, err, "Error during unmarshalling: %v", err)
	assert.NotEqualf(t, body[key], "", "Key '%v' does not exists: %v", key, body)
	assert.Equalf(t, len(body), 1, "Key '%v' is not alone: %v", key, body)
}

func zoneFileIsNotCreated(t *testing.T, domain string) {
	_, err := os.Open("./flap.zones.d/db." + domain)
	assert.Truef(t, errors.Is(err, os.ErrNotExist), "The zone file should not be created")
}
