package main

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/autotls"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/joho/godotenv"
)

// FlapStatus -
type FlapStatus struct {
	Code    int
	Error   error
	Message string
}

// MockEmailCount - Allow mocking allowed domain count for email during test
var MockEmailCount map[string]int

func main() {
	// Load .env
	err := godotenv.Load(".env", "release.env")
	log.Println(err)

	if err != nil && !errors.Is(err, os.ErrNotExist) {
		log.Println(err)
		panic("failed to load env var")
	}

	// Customize logs
	log.SetPrefix("[FLAP_DNS] ")
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	// Create new router
	router := gin.Default()

	InitDB()
	setupSentry(router)
	setupValidators()
	setupRoutes(router)

	if gin.Mode() == gin.ReleaseMode {
		err = autotls.Run(router, "flap.id")
	} else {
		err = router.Run()
	}

	if err != nil {
		log.Println(err)
	}

}

func setupRoutes(router *gin.Engine) {
	router.POST("/domains", Domains{}.Post)
	router.PATCH("/domains/:name", Domains{}.Patch)
	router.DELETE("/domains/:name", Domains{}.Delete)
	router.GET("/tokens/:email", Tokens{}.Get)
}

func setupValidators() {
	// Get validation engine
	v, ok := binding.Validator.Engine().(*validator.Validate)
	if !ok {
		panic("can not get validator")
	}

	// Create isflapid validator
	err := v.RegisterValidation("isflapid", IsFlapID)
	if err != nil {
		panic(err)
	}

	// Create isnotinflapidblacklist validator
	err = v.RegisterValidation("isnotinflapidblacklist", IsNotInFlapIDBlacklist)
	if err != nil {
		panic(err)
	}
}

func setupSentry(router *gin.Engine) {
	if gin.Mode() == gin.DebugMode {
		return
	}

	log.Println("Registering Sentry")

	// To initialize Sentry's handler, you need to initialize Sentry itself beforehand
	err := sentry.Init(sentry.ClientOptions{
		Dsn:              "https://" + os.Getenv("SENTRY_KEY") + "@sentry.io/4469120",
		Debug:            true,
		AttachStacktrace: true,
		Release:          os.Getenv("RELEASE_ID"),
	})

	if err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}

	// Once it's done, you can attach the handler as one of your middleware
	router.Use(sentrygin.New(sentrygin.Options{
		Repanic: true,
	}))
}
