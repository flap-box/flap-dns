#!/bin/bash

set -eux

# Create dummy flap-dns file in case it does not exists
touch /opt/flap-dns

# Swap old and new flap-dns binaries
mv /opt/flap-dns /opt/flap-dns.old
mv /opt/flap-dns.new /opt/flap-dns

# Write RELEASE_ID to the release.env file
echo "RELEASE_ID=$RELEASE_ID" > /opt/release.env

# Restart flap-dns
systemctl restart flap-dns

# Wait 5 second before getting the status
sleep 5
if ! systemctl status flap-dns
then
	# Swap back flap-dns binaries
	rm /opt/flap-dns
	mv /opt/flap-dns.old /opt/flap-dns
	systemctl restart flap-dns

	exit 1
fi

rm /opt/flap-dns.old
