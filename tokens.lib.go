package main

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/gomail.v2"
)

// Token - Structure of a token in the database
type Token struct {
	gorm.Model
	Token       string `gorm:"size:60;not null;unique"`
	Email       string `gorm:"not null"`
	CreatedDate time.Time
}

// GenerateToken - Generate a new token and return the token and its hash
func GenerateToken() (string, string, error) {
	var tokenBytes [15]byte

	_, err := rand.Read(tokenBytes[:])

	if err != nil {
		return "", "", err
	}

	token := hex.EncodeToString(tokenBytes[:])
	hash, err := hashToken(token)

	return token, hash, err
}

func hashToken(token string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(token), 14)
	return string(bytes), err
}

// TokenEqualHash - Check a token and a hash equality
func TokenEqualHash(token, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(token))
	return err == nil
}

// GetPendingTokenInfo - Check that the token give the right to register a domain
// This will simply check the exitance of the token in the tokens table and that create_date is less than a day ago
func GetPendingTokenInfo(token string) (Token, string, error) {
	var tokensInfo []Token

	db := GetDBConnection()
	defer db.Close()

	yesterday := time.Now().AddDate(0, 0, -1)
	db.Where("created_date > ?", yesterday).Find(&tokensInfo)

	if db.Error != nil {
		return Token{}, "", db.Error
	}

	// Return a tokenInfo if an equality is found
	for _, tokenInfo := range tokensInfo {
		if TokenEqualHash(token, tokenInfo.Token) {
			return tokenInfo, tokenInfo.Token, nil
		}
	}

	hash, err := hashToken(token)

	if err != nil {
		return Token{}, "", err
	}

	return Token{}, hash, nil
}

type templateToken struct {
	Hash   string
	Domain string
	Empty  bool
}

type templateParam struct {
	Tokens []templateToken
}

// SendTokens - Send the passed tokens to an email address
func SendTokens(to string, tokensMap map[string]string) {
	// Don't send mail during tests
	if flag.Lookup("test.v") != nil || gin.Mode() == gin.DebugMode {
		return
	}

	var tokensList []templateToken
	for key, token := range tokensMap {
		tokensList = append(tokensList, templateToken{
			Hash:   token,
			Domain: key,
			Empty:  strings.HasPrefix(key, "Empty "),
		})
	}

	t, err := template.ParseFiles("tokens-mail.template.html")
	if err != nil {
		log.Println(err)
		panic("failed to load mail template")
	}
	var mailContent bytes.Buffer
	err = t.Execute(&mailContent, templateParam{Tokens: tokensList})
	if err != nil {
		log.Println(err)
		panic("failed to generate mail content")
	}

	fmt.Println("Connecting to " + os.Getenv("TOKEN_EMAIL_SMTP_SERVER") + ":" + os.Getenv("TOKEN_EMAIL_SMTP_PORT"))

	port, err := strconv.Atoi(os.Getenv("TOKEN_EMAIL_SMTP_PORT"))
	if err != nil {
		log.Println(err)
		panic("failed get mail server params")
	}

	dialer := gomail.NewDialer(
		os.Getenv("TOKEN_EMAIL_SMTP_SERVER"),
		port,
		os.Getenv("TOKEN_EMAIL_USERNAME"),
		os.Getenv("TOKEN_EMAIL_PASSWORD"),
	)

	message := gomail.NewMessage()
	message.SetHeader("From", os.Getenv("TOKEN_EMAIL_USERNAME")+"@"+os.Getenv("TOKEN_EMAIL_SMTP_SERVER"))
	message.SetHeader("To", to)
	message.SetHeader("Subject", "Vos tokens flap.id")
	message.SetBody("text/html", mailContent.String())

	// Send the email
	err = dialer.DialAndSend(message)
	if err != nil {
		log.Println(err)
		panic("failed to send the mail")
	}

	fmt.Println("Mail send")
}
