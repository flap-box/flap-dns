package main

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"text/template"
	"time"

	"github.com/miekg/dns"
)

type zone struct {
	Name   string
	IP4    string
	IP6    string
	DKIM   string
	Serial string
}

func getZonePath(domainname string) string {
	// use local folder during tests
	if flag.Lookup("test.v") != nil {
		err := os.MkdirAll("./flap.zones.d", os.ModePerm)

		if err != nil {
			panic(err)
		}

		return "./flap.zones.d/db." + domainname
	}

	return "/var/cache/bind/flap.zones.d/db." + domainname
}

// GenerateZone - Generate a zone file from a domain structure
func GenerateZone(domain *Domain) error {
	t, err := template.ParseFiles("zone.template")

	if err != nil {
		return err
	}

	file, err := os.Create(getZonePath(domain.Name))
	defer file.Close()

	if err != nil {
		return err
	}

	// Increment serial, or reset to 0 for the new day.
	today := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)
	if domain.SerialDate == today {
		domain.Serial++
	} else {
		domain.Serial = 0
		domain.SerialDate = today
	}
	db := GetDBConnection()
	defer db.Close()

	db.Save(domain)

	// Panic if serial is 100 because it can not be more than 99.
	// The serial is of the form YYYYMMDD+counter.
	// But the total number can not be more than the max 32 bit integer (4294967296).
	// This cap the counter to 99. See below for visual explaination:
	// max int => 4294 96 72 96
	// serial  => YYYY MM DD CC
	if domain.Serial >= 100 {
		panic("too many changes for today.")
	}

	// Compute new zone serial.
	serial := today.Format("20060102")
	if domain.Serial <= 9 {
		serial += "0"
	}
	serial += strconv.Itoa(domain.Serial)

	log.Println("Writting in " + file.Name())

	err = t.Execute(file, zone{
		Name:   domain.Name,
		IP4:    domain.IP4,
		IP6:    domain.IP6,
		DKIM:   splitDKIM(domain.DKIM),
		Serial: serial,
	})

	if err != nil {
		return err
	}

	log.Println("Adding zone domain for: " + domain.Name)
	err = addDomainZone(domain.Name)

	if err != nil {
		return err
	}

	log.Println("Adding domain to the catalog for: " + domain.Name)
	err = addDomainToCatalog(domain.Name)

	if err != nil {
		return err
	}

	return nil
}

// Split a DKIM into 255 chars strings including two quotes
// See: https://kb.isc.org/docs/aa-00356
func splitDKIM(DKIM string) string {
	if len(DKIM) == 0 {
		return ""
	}

	splittedDKIM := "\""
	for i, char := range DKIM {
		if i != 0 && i%253 == 0 {
			splittedDKIM += "\" \""
		}

		splittedDKIM += string(char)
	}

	if splittedDKIM[len(splittedDKIM)-1] != '"' {
		splittedDKIM += "\""
	}

	return splittedDKIM
}

// Run the rndc cmd to add the new zone
func addDomainZone(domainName string) error {
	// Don't send mail during tests
	if flag.Lookup("test.v") != nil {
		return nil
	}

	if !dns.IsFqdn(domainName + ".") {
		return fmt.Errorf("the domain must be a FQDN: " + domainName)
	}

	_, err := exec.Command("rndc", "zonestatus", domainName).Output()

	// If there is no error, it means the zone already exist.
	exist := err == nil

	if exist {
		_, err = exec.Command("rndc", "freeze", domainName).Output()
		if err != nil {
			panic(err)
		}
		_, err = exec.Command("rndc", "reload", domainName).Output()
		if err != nil {
			panic(err)
		}
		_, err = exec.Command("rndc", "thaw", domainName).Output()
		if err != nil {
			panic(err)
		}
	} else {
		_, err := exec.Command("rndc", "addzone", domainName, "{type master; file \""+getZonePath(domainName)+"\";};").Output()
		if err != nil {
			if ee, ok := err.(*exec.ExitError); ok {
				log.Println(string(ee.Stderr))
			}
			panic(err)
		}
	}

	return nil
}

// Run the rndc cmd to add the new zone
func removeDomainZone(domainName string) error {
	// Don't send mail during tests
	if flag.Lookup("test.v") != nil {
		return nil
	}

	if !dns.IsFqdn(domainName + ".") {
		return fmt.Errorf("the domain must be a FQDN: " + domainName)
	}

	cmd := exec.Command("rndc", "delzone", domainName)
	_, err := cmd.Output()

	if err != nil {
		log.Println(cmd)
		if ee, ok := err.(*exec.ExitError); ok {
			log.Println(string(ee.Stderr))
		}
		panic(err)
	}

	return nil
}

// This wil use DynDNS to update the catalog zone
func addDomainToCatalog(domainName string) error {
	// Don't send mail during tests
	if flag.Lookup("test.v") != nil {
		return nil
	}

	// Create client with the tsig key
	client := new(dns.Client)
	client.TsigSecret = map[string]string{"flap-dns.": os.Getenv("TSIG_KEY")}

	// Compute sha1 hash of the domain name
	sha1Hasher := sha1.New()
	_, err := sha1Hasher.Write([]byte(domainName))

	if err != nil {
		return err
	}

	hash := hex.EncodeToString(sha1Hasher.Sum(nil))

	// Create a new message to add a PTR record for the domain in catalog.flap.id.
	// Create update message
	message := new(dns.Msg)
	message.SetUpdate("catalog.flap.id.")

	// Create record
	dnsRecord := new(dns.PTR)
	dnsRecord.Hdr = dns.RR_Header{Name: hash + ".zones.catalog.flap.id.", Rrtype: dns.TypePTR, Class: dns.ClassINET, Ttl: 3600}
	dnsRecord.Ptr = domainName + "."

	// Add insert cmd in the message
	message.Insert([]dns.RR{dnsRecord})

	// Sign message
	message.SetTsig("flap-dns.", dns.HmacSHA256, 300, time.Now().Unix())

	// Send the message
	_, _, err = client.Exchange(message, "ns1.flap.id:53")

	if err != nil {
		return err
	}

	return nil
}

// This wil use DynDNS to update the catalog zone
func removeDomainFromCatalog(domainName string) error {
	// Don't send mail during tests
	if flag.Lookup("test.v") != nil {
		return nil
	}

	// Create client with the tsig key
	client := new(dns.Client)
	client.TsigSecret = map[string]string{"flap-dns.": os.Getenv("TSIG_KEY")}

	// Compute sha1 hash of the domain name
	sha1Hasher := sha1.New()
	_, err := sha1Hasher.Write([]byte(domainName))

	if err != nil {
		return err
	}

	hash := hex.EncodeToString(sha1Hasher.Sum(nil))

	// Create a new message to add a PTR record for the domain in catalog.flap.id.
	// Create update message
	message := new(dns.Msg)
	message.SetUpdate("catalog.flap.id.")

	// Create record
	dnsRecord := new(dns.PTR)
	dnsRecord.Hdr = dns.RR_Header{Name: hash + ".zones.catalog.flap.id.", Rrtype: dns.TypePTR, Class: dns.ClassINET, Ttl: 3600}
	dnsRecord.Ptr = domainName + "."

	// Add remove cmd in the message
	message.RemoveRRset([]dns.RR{dnsRecord})

	// Sign message
	message.SetTsig("flap-dns.", dns.HmacSHA256, 300, time.Now().Unix())

	// Send the message
	_, _, err = client.Exchange(message, "ns1.flap.id:53")

	if err != nil {
		return err
	}

	return nil
}

// DeleteZone - Delete a zone file
func DeleteZone(domainName string) error {
	err := removeDomainZone(domainName)

	if err != nil {
		return nil
	}

	err = removeDomainFromCatalog(domainName)

	if err != nil {
		return nil
	}

	err = os.Remove(getZonePath(domainName))

	if errors.Is(err, os.ErrNotExist) {
		return nil
	}

	return err
}
