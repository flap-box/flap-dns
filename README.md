# FLAP's DynDNS API for `flap.id` domain names

Allows FLAP's consumers to register a `flap.id` domain name.

---

### Workflow

1. A consumer buys a FLAP box and submit his email address.
2. The consumer ask this server for the token he can use to register a `flap.id` domain (`GET /tokens/:email`).
3. This server send an email to the consumer containing the token.
4. The consumer register a new domain (`POST /domains`).
5. This server link the token the email and the domain name in a sqlite database.
6. The consumer set the IP address and DKIM info (`PATCH /domains/:domainName`)
7. This server generate a zone file, copy this zone file to the bind9 config directory and make a request to the bind9 server to add the domain to the catalog zone.

Now, the domain name is configured and the consumer can:

- Modify the IP or DKIM info.
- Rename the domain.
- Delegate the domain to another email.

### Architecture

The `flap.id` domains are serve with two [bind9](https://bind9.readthedocs.io/en/latest) server: a master,`ns1.flap.id` and slave, `ns2.flap.id`.
The update mechanism works with [catalogs zones](https://bind9.readthedocs.io/en/latest/advanced.html#catalog-zones).
The allowed number of domains by email is fetched from Stripe's API.

### Monitoring

Monitoring is made with [Prometheus](https://prometheus.io/) and [Sentry](https://sentry.io).

- [Install guide](https://www.digitalocean.com/community/tutorials/how-to-install-prometheus-on-ubuntu-16-04) for Prometheus.
- [Bind exporter](https://github.com/prometheus-community/bind_exporter) for Prometheus.

#### Files and Directories

- `/opt/flap-dns`. `flap-dns` binary.
- `/opt/flap-dns.db`. SQLite database containing tokens and domains information.
- `/etc/bind/named.conf.options`. Bind9 configuration file. See `bind9.{master,slave}.conf` in this repo.
- `/etc/systemd/system/flap-dns.service`. Systemd's service for `flap-dns`. See `flap-dns.service` in this repo.
- `/var/cache/bind/flap.zones.d`. Contains zones files.

### REST API

| METHOD | Path            | Body                         | Action                                             |
| ------ | --------------- | ---------------------------- | -------------------------------------------------- |
| GET    | /tokens/{email} |                              | Send, by email, tokens associated to the email.    |
| POST   | /domains        | `{token, name}`              | Register a new domain.                             |
| PATCH  | /domains/{name} | `{token, ip4?, ip6?, dkim?}` | Update the IP and DKIM of the domain.              |
| PATCH  | /domains/{name} | `{token, name}`              | Migrate the domain name to the give domain.        |
| PATCH  | /domains/{name} | `{token, delegate}`          | Delegate the domain management to the given email. |

### Contributing

##### Resources

- [go-stripe](https://github.com/stripe/stripe-go)
  - [API Docs](https://stripe.com/docs/api)
  - [go-doc](https://godoc.org/github.com/stripe/stripe-go)
