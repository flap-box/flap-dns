package main

import (
	"flag"
	"os"

	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"
	"github.com/stripe/stripe-go/paymentintent"
	"github.com/stripe/stripe-go/subschedule"
)

func getStripeClient() {
	stripe.Key = os.Getenv("STRIPE_KEY")

	// if gin.Mode() == gin.DebugMode {
	// 	stripe.DefaultLeveledLogger = &stripe.LeveledLogger{
	// 		Level: stripe.LevelDebug,
	// 	}
	// }

	// stripe.SetBackend("api", stripe.Backend) // optional, useful for mocking

}

// GetAllowedDomainsForStripeEmail - Get the allowed nopmber of domain an email can register
// TODO: check that there is no conflict when the email is a delegate
func GetAllowedDomainsForStripeEmail(email string) (int, error) {
	// Use mock data during tests.
	if flag.Lookup("test.v") != nil {
		return MockEmailCount[email], nil
	}

	getStripeClient()

	i := customer.List(&stripe.CustomerListParams{
		Email: &email,
	})

	count := 0

	for i.Next() {
		cust := i.Customer()

		// Count active subscriptions.
		for _, sub := range cust.Subscriptions.Data {
			if sub.Status == stripe.SubscriptionStatusActive || sub.Status == stripe.SubscriptionStatusTrialing {
				count++
			}
		}

		// Count scheduled subscriptions.
		k := subschedule.List(&stripe.SubscriptionScheduleListParams{
			Customer: cust.ID,
		})

		for k.Next() {
			count++
		}

		// Count payments from the user.
		j := paymentintent.List(&stripe.PaymentIntentListParams{
			Customer: &cust.ID,
		})

		for j.Next() {
			pi := j.PaymentIntent()
			if pi.Status == stripe.PaymentIntentStatusSucceeded && len(pi.Charges.Data[0].Refunds.Data) == 0 {
				count++
			}
		}
	}

	return count, nil
}
