package main

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// GetDBConnection - Get a connection to the sqlite database
func GetDBConnection() *gorm.DB {
	// Connect to db.
	db, err := gorm.Open("sqlite3", "flap-dns.db")

	if err != nil {
		log.Println(err)
		panic("DB connection error")
	}

	return db
}

// InitDB - Try to connect to the sqlite database and run the migrations
func InitDB() {
	log.Println("Init DB")

	db := GetDBConnection()
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Domain{})
	db.AutoMigrate(&Token{})
}

// CleanTokens - Clean dangling tokens in the tokens table
func CleanTokens() {
	// Soft delete old token
	yesterday := time.Now().AddDate(0, 0, -1)

	db := GetDBConnection()
	defer db.Close()

	db.Where("created_date < ?", yesterday).Delete(&Token{})

	// Remove deleted tokens
	db.Unscoped().Delete(&Token{})
}
